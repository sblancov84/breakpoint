# Breakpoint

Breakpoint is a proof of concept (PoC) to test how to debug with the new
debugger built-in function of the python 3.7 version inside a docker container.

It is a success! I am going to explain how to.

# How to

## Requirements

You should have python 3.7 and docker and docker-compose installed before to
run this. I have tested this on a Debian 10, so probably you need at least an
UNIX environment.

Make is useful too.

# Run

Easy steps thanks to make:

    make run

Or you can make the same typing on your terminal:

    docker-compose up -d
    python -m webbrowser -t http://localhost:5555

Or even more manually:

    docker-compose up -d
    # open your favourite web browser and type localhost:5555 to see web-pdb

If you want to test it without install docker... well, I recommend you learn
docker! but my recommendation is not helpful right now, so:

    pip install -r requirements/requirements.txt
    PYTHONBREAKPOINT=web_pdb.set_trace python app/main.py
    google-chrome http://localhost:5555 # or firefox, chromium, opera...

NOTE: I recommend to use virtualenvs too, to mantain your environment as
cleanest as possible if you do not use docker.
