create-virtualenv:
	pyenv virtualenv 3.7.3 breakpoint

install:
	pip install -r requirements/requirements.txt

pdb-debugger:
	python app/main.py

ipdb-debugger:
	PYTHONBREAKPOINT=ipdb.set_trace python app/main.py

webpdb-debugger:
	PYTHONBREAKPOINT=web_pdb.set_trace python app/main.py

run:
	docker-compose up -d
	python -m webbrowser -t http://localhost:5555

stop:
	docker-compose down
